import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import {
  View,
  Dimensions,
  StyleSheet,
  ImageBackground,
  Button,
  TextInput,
  Image,
  Text,
  BackHandler,
  ActivityIndicator
} from "react-native";
import FadeInView from "../components/FadeInView";
import AsyncStorage from "@react-native-community/async-storage";

import { Api } from "../constants/api";
import { setUser } from "../reducers/UserActions";

class RegisterScreen extends Component {
  static navigationOptions = {
    header: null
  };

  state = {
    username: "",
    email: "",
    password: "",
    isLoading: false
  };

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = async () => {
    await this.props.navigation.navigate("Auth");
  }

  render() {
    return (
      <ImageBackground
        source={require("../assets/images/intro.jpg")}
        style={styles.container}
      >
        <FadeInView duration={1000}>
          <Image
            resizeMode="cover"
            source={require("../assets/images/DetectEat.png")}
            style={styles.title}
          />
          <TextInput
            style={styles.textInput}
            placeholder="User Name"
            onChangeText={username => this.setState({ username })}
            value={this.state.username}
          />
          <TextInput
            style={styles.textInput}
            placeholder="Email"
            onChangeText={email => this.setState({ email })}
            value={this.state.email}
          />
          <TextInput
            style={styles.textInput}
            placeholder="Password"
            secureTextEntry={true}
            onChangeText={password => this.setState({ password })}
            value={this.state.password}
          />
          <View style={styles.loginButton}>
            <Button
              title="Register"
              color="#841584"
              onPress={this._onRegister}
            />
          </View>
        </FadeInView>
        {this.state.isLoading && (
          <View style={styles.loadingContainer}>
            <Text style={styles.loadingText}>
              Please wait, it may take a few seconds...
            </Text>
            <ActivityIndicator size="large" />
          </View>
        )}
      </ImageBackground>
    );
  }

  _onRegister = async () => {
    const { username, email, password } = this.state;

    if (!username || !password) {
      alert("Please enter user name and password");
      return;
    }

    if (username.length < 2 || username.length > 8) {
      alert("User name must have at least 2 characters and max of 8 characters");
      return;
    }

    if (password.length < 2) {
      alert("Password name must have at least 2 characters");
      return;
    }

    if (!validateEmail(email)) {
      alert("Invalid email address");
      return;
    }

    this.setState({ isLoading: true });

    await Api.post("/auth/local/register", { username, email, password })
      .then(async ({ token, user }) => {
        await AsyncStorage.setItem("localToken", token);
        this.props.setUser({ ...user , fbImage: null });
        this.setState({ isLoading: false });
        this.props.navigation.navigate("App");
      })
      .catch(error => {
        this.setState({ isLoading: false });
        alert(error);
      });

  };
}

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setUser }, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(RegisterScreen);

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "black",
    color: "#fff"
  },
  title: {
    width: 142,
    height: 150,
    marginVertical: 15,
    alignSelf: "center"
  },
  textInput: {
    height: 40,
    width: 250,
    borderColor: "gray",
    borderWidth: 1,
    color: "black",
    backgroundColor: "#fff",
    marginVertical: 5
  },
  loginButton: {
    height: 30,
    width: 250,
    marginVertical: 5
  },
  loadingContainer: {
    position: "absolute",
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    backgroundColor: "rgba(0,0,0,0.7)",
    alignItems: "center",
    justifyContent: "center"
  },
  loadingText: {
    color: "#ffffff",
    fontSize: 18,
    fontFamily: "Comfortaa-Bold",
    marginBottom: 15,
    marginHorizontal: 50,
    textAlign: "center",
    lineHeight: 30
  },
});
