import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import { Api } from "../constants/api";
import { setUser } from "../reducers/UserActions";

import {
  View,
  Dimensions,
  StyleSheet,
  ImageBackground,
  Button,
  TextInput,
  Text,
  Image,
  ActivityIndicator
} from "react-native";

import FBLoginButton from "../components/FBLoginButton";
import FadeInView from "../components/FadeInView";
import AsyncStorage from "@react-native-community/async-storage";
import getFacebookUser from "../utility/getFacebookUser";

class LoginScreen extends Component {
  static navigationOptions = {
    header: null
  };

  state = {
    username: "",
    password: "",
    isLoading: false
  };

  render() {
    return (
      <ImageBackground
        source={require("../assets/images/intro.jpg")}
        style={styles.container}
      >
        <FadeInView duration={1000}>
          <Image
            resizeMode="cover"
            source={require("../assets/images/DetectEat.png")}
            style={styles.title}
          />
          <TextInput
            style={styles.textInput}
            placeholder="User name"
            onChangeText={username => this.setState({ username })}
            value={this.state.username}
          />
          <TextInput
            style={styles.textInput}
            placeholder="Password"
            secureTextEntry={true}
            onChangeText={password => this.setState({ password })}
            value={this.state.password}
          />
          <View style={styles.loginButton}>
            <Button
              title="Log in"
              color="#6fe073"
              onPress={this._onLocalLogin}
            />
          </View>
          <View style={styles.loginButton}>
            <Button
              title="Register"
              color="#841584"
              onPress={this._onRegister}
            />
          </View>
          <FBLoginButton
            style={styles.fbButton}
            onLogin={this._onFacebookLogin}
          />
        </FadeInView>
        {this.state.isLoading && (
          <View style={styles.loadingContainer}>
            <Text style={styles.loadingText}>
              Please wait, it may take a few seconds...
            </Text>
            <ActivityIndicator size="large" />
          </View>
        )}
      </ImageBackground>
    );
  }

  _onLocalLogin = async () => {
    const { username, password } = this.state;

    if (!username || !password) {
      alert("Please enter user name and password");
      return;
    }

    if (username.length < 2 || username.length > 8) {
      alert(
        "User name must have at least 2 characters and max of 8 characters"
      );
      return;
    }

    if (password.length < 2) {
      alert("Password name must have at least 2 characters");
      return;
    }

    this.setState({ isLoading: true });

    await Api.post("/auth/local/login", { username, password })
      .then(async ({ token, user }) => {
        await AsyncStorage.setItem("localToken", token);
        user.fbImage = null;
        await this._onLogin(user);
      })
      .catch(error => {
        alert("Login Failed");
        this.setState({ isLoading: false });
      });
  };

  _onFacebookLogin = async token => {
    try {
      const user = await getFacebookUser(token);
      await AsyncStorage.setItem("facebookToken", token);
      this._onLogin(user);
    } catch {
      alert("Login Failed");
    }
  };

  _onRegister = () => this.props.navigation.navigate("Register");

  _onLogin = async user => {
    user.meals = await Api.get(`/api/meal/${user.email}`);
    this.props.setUser(user);
    this.setState({ isLoading: false });
    this.props.navigation.navigate("App");
  };
}

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setUser }, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(LoginScreen);

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "black",
    color: "#fff"
  },
  title: {
    width: 142,
    height: 150,
    marginVertical: 15,
    alignSelf: "center"
  },
  textInput: {
    height: 40,
    width: 250,
    borderColor: "gray",
    borderWidth: 1,
    color: "black",
    backgroundColor: "#fff",
    marginVertical: 3
  },
  loginButton: {
    height: 30,
    width: 250,
    marginVertical: 6
  },
  fbButton: {
    height: 30,
    width: 250,
    marginVertical: 10,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center"
  },
  loadingContainer: {
    position: "absolute",
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    backgroundColor: "rgba(0,0,0,0.7)",
    alignItems: "center",
    justifyContent: "center"
  },
  loadingText: {
    color: "#ffffff",
    fontSize: 18,
    fontFamily: "Comfortaa-Bold",
    marginBottom: 15,
    marginHorizontal: 50,
    textAlign: "center",
    lineHeight: 30
  },
  inputs: {
    width: 250,
    alignItems: 'center'
  },
  inputWrapper: {
    padding: 0,
    margin: 0,
  },
  inputContainer: {
    width: '100%',
    marginVertical: 5,
    marginHorizontal: 0,
    paddingHorizontal: 0,
    borderWidth: 1,
    borderColor: "#6fe073",
    borderRadius: 10
  },
  input: {
    paddingLeft: 10,
    color: "#6fe073",
    fontFamily: "Comfortaa-Bold"
  },
  inputLabel: {
    color: "#6fe073",
  }
});
