import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { StyleSheet, View, ScrollView, Text, Image, TouchableOpacity  } from "react-native";
import { AccordionList } from "accordion-collapse-react-native";
import { Icon, SearchBar } from "react-native-elements";
import LinearGradient from "react-native-linear-gradient";
import Dialog from "react-native-dialog";
import { withNavigationFocus } from "react-navigation";
import Tts from 'react-native-tts';

import { deleteMeal } from "../reducers/UserActions";
import Ingredients from "../components/Ingredients";
import { Api } from "../constants/api";

import FlashMessage from 'react-native-flash-message';
import { showMessage } from 'react-native-flash-message';

const LIGHT_COLOR = "#f093fb";
const DARK_COLOR = "#f5576c";

class HistoryScreen extends React.PureComponent {
  static navigationOptions = {
    header: null
  };

  state = {
    dialogVisible: false,
    selectedMealId: null,
    search: "",
    filteredMeals: this.props.meals
  };

  componentDidMount() {
    if (this.props.isFocused && this.props.speakMode) this._speak();
  }  

  componentDidUpdate(prevProps) {
    if (prevProps === this.props) return;

    this.setState({
      filteredMeals: this.props.meals.filter(_ =>
        _.name.toUpperCase().includes(this.state.search.toUpperCase())
      )
    });

    if (this.props.isFocused && !prevProps.isFocused && this.props.speakMode) this._speak();
  }

  render() {
    return (
      <View style={styles.container}>
        <LinearGradient
          colors={[LIGHT_COLOR, DARK_COLOR]}
          style={styles.linearGradient}
        >
          <View style={styles.menu}>
            <Icon
              name="bars"
              type="font-awesome"
              color="#fff"
              onPress={this._onMenuClick}
              underlayColor={"transparent"}
            />
            <Text style={styles.headerText}>Recent Meals</Text>
          </View>
          <View>
            <SearchBar
              round
              placeholder="Search..."
              onChangeText={this._updateSearch}
              onClear={this._clearSearch}
              value={this.state.search}
              containerStyle={styles.searchBarContainer}
              inputContainerStyle={styles.searchBarInputContainer}
              inputStyle={styles.searchText}
            />
          </View>
        </LinearGradient>
        {this.props.meals && this.props.meals.length !== 0 ? (
          <ScrollView>
            <AccordionList
              list={this.state.filteredMeals}
              header={this._head}
              body={this._body}
            />
            <Dialog.Container visible={this.state.dialogVisible}>
              <Dialog.Title>Delete Meal</Dialog.Title>
              <Dialog.Description>
                Do you want to delete this meal? You cannot undo this action.
              </Dialog.Description>
              <Dialog.Button label="Cancel" onPress={this.handleCancel} />
              <Dialog.Button label="Delete" onPress={this.handleDelete} />
            </Dialog.Container>
          </ScrollView>
        ) : (
          <View style={styles.message}>
            <Text style={styles.messageText}>
              No Meals has been taking yet...
            </Text>
            <Text style={styles.takePhoto} onPress={this._takePhoto}>
              Take your first one now!
            </Text>
          </View>
        )}
        <View style={styles.addBtn}>
          <Icon
            reverse
            name="add"
            color={DARK_COLOR}
            onPress={this._takePhoto}
            underlayColor={"transparent"}
          />
        </View>
        <FlashMessage duration={1000} ref="deleteMessage"/>
      </View>
    );
  }

  _onMenuClick = () => this.props.navigation.openDrawer();

  _clearSearch = () =>
    this.setState({ search: "", filteredMeals: this.props.meals });

  _updateSearch = search => {
    this.setState({
      search,
      filteredMeals: this.props.meals.filter(_ =>
        _.name.toUpperCase().includes(search.toUpperCase())
      )
    });
  };

  _takePhoto = () => this.props.navigation.navigate("Photo");

  _speak() {
    Tts.speak("You arrived to your meals history page.");

    if (this.props.meals && this.props.meals.length) return;
    
    Tts.speak("No Meals has been taking yet");
  }

  _head = ({ _id, time, name, food, formattedImage }) => {
    return (
      <View style={styles.accordionHeader} key={name} onPress={() => this._onMealClick(name, food)}>
        <View style={styles.imageCloseContainer}>
          <View style={styles.closeBtn}>
            <Icon
              name="close"
              color={DARK_COLOR}
              onPress={() => this.showDialog(_id)}
              underlayColor={"transparent"}
            />
          </View>
          <Image
            source={{ uri: `data:image/jpg;base64,${formattedImage}` }}
            resizeMode="cover"
            style={styles.image}
          />
        </View>
        <View style={styles.accordionHeaderDetails}>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.details}>{this._calcCalories(food)} kl</Text>
          <View style={styles.time}>
            <Icon
              name="access-time"
              color={DARK_COLOR}
              underlayColor={"transparent"}
            />
            <Text style={styles.timeText}>{new Date(time).toDateString()}</Text>
          </View>
        </View>
      </View>
    );
  };

  _calcCalories(food) {
    let calories = 0;
    food.map(({ contents }) => {
      calories += Number.parseInt(contents.calories);
    });
    return calories;
  }

  showDialog = mealId =>
    this.setState({ dialogVisible: true, selectedMealId: mealId });

  handleCancel = () =>
    this.setState({ dialogVisible: false, selectedMealId: null });

  handleDelete = async () => {
    try {
      await Api.delete(`/api/meal/${this.state.selectedMealId}`);
      this.props.deleteMeal(this.state.selectedMealId);

      showMessage({
        message: 'Delete Success!',
        type: 'success'
      });

    } catch {
      alert("Failed deleting meal");
    }

    this.setState({ dialogVisible: false, selectedMealId: null });
  };

  _body = ({ food }) => <Ingredients food={food} />;

  _onMealClick = (name, food) => {
    if (!this.props.speakMode) return;

    Tts.speak(`The meal is ${name}`);
    Tts.speak(`The ingredients are: `);
    food.map(food =>  Tts.speak(`${food.name}`));
  }
}

const HistoryScreenWithNavigation = withNavigationFocus(HistoryScreen);

const mapStateToProps = state => ({ meals: state.user.meals, speakMode: state.user.speakMode });

const mapDispatchToProps = dispatch =>
  bindActionCreators({ deleteMeal }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HistoryScreenWithNavigation);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column"
  },
  linearGradient: {
    width: "100%",
    flexDirection: "column",
    borderBottomColor: "#e0e0e0",
    borderBottomWidth: 2
  },
  headerText: {
    color: "#ffffff",
    marginLeft: 10,
    fontSize: 17,
    fontFamily: "Comfortaa-Bold"
  },
  menu: {
    margin: 15,
    marginBottom: 10,
    flexDirection: "row",
    alignItems: "center"
  },
  searchBarContainer: {
    backgroundColor: "transparent",
    borderBottomColor: "transparent",
    borderTopColor: "transparent"
  },
  searchBarInputContainer: {
    backgroundColor: "#fff"
  },
  searchText: {
    fontSize: 16,
    fontFamily: "Comfortaa-Bold"
  },
  message: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    fontFamily: "Comfortaa-Bold"
  },
  messageText: {
    fontFamily: "Comfortaa-Bold"
  },
  takePhoto: {
    marginTop: 20,
    alignSelf: "center",
    fontSize: 18,
    fontFamily: "Comfortaa-Bold"
  },
  accordionHeader: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderBottomColor: "#e0e0e0",
    borderBottomWidth: 1,
    paddingHorizontal: 10
  },
  imageCloseContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  image: {
    width: 100,
    height: 75,
    margin: 12,
    borderColor: "#e0e0e0",
    borderWidth: 1
  },
  name: {
    fontSize: 20,
    fontFamily: "Comfortaa-Bold"
  },
  details: {
    fontSize: 15,
    marginBottom: 5,
    fontFamily: "Comfortaa-Regular"
  },
  time: {
    flexDirection: "row",
    alignItems: "center"
  },
  timeText: {
    color: DARK_COLOR,
    fontFamily: "Comfortaa-Regular",
    marginLeft: 5
  },
  accordionHeaderDetails: {
    marginLeft: 10,
    width: "70%"
  },
  addBtn: {
    position: "absolute",
    bottom: 5,
    right: 5
  }
});
