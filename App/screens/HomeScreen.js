import React from "react";
import { connect } from "react-redux";
import {
  Image,
  Platform,
  StyleSheet,
  View,
  ImageBackground,
  TouchableOpacity
} from "react-native";
import Tts from 'react-native-tts';
import { Icon } from "react-native-elements";
import { withNavigationFocus } from "react-navigation";
import * as Animatable from "react-native-animatable";

class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    if (this.props.isFocused && this.props.speakMode) this._speak();
  }  

  componentDidUpdate(prevProps) {
    if (!prevProps.isFocused && this.props.isFocused && this.props.speakMode) this._speak();
  }  

  render() {
    return (
      <ImageBackground
        source={require("../assets/images/intro.jpg")}
        style={styles.container}
      >
        <View style={styles.menu}>
          <Icon
            name="bars"
            type="font-awesome"
            color="#fff"
            onPress={this._onMenuClick}
            underlayColor={"transparent"}
          />
        </View>
        <TouchableOpacity onPress={this._takePhoto} style={styles.title}>
          <Animatable.View
            animation="pulse"
            easing="ease-out"
            duration={2000}
            iterationCount="infinite"
          >
            <Image
              resizeMode="cover"
              source={require("../assets/images/DetectEat.png")}
              style={styles.image}
            />
          </Animatable.View>
        </TouchableOpacity>
      </ImageBackground>
    );
  }

  _onMenuClick = () => this.props.navigation.openDrawer();

  _takePhoto = () => this.props.navigation.navigate("Photo");

  _speak = () => Tts.speak(`You arrived to home screen!`);
}

const HomeScreenWithNavigation = withNavigationFocus(HomeScreen);

const mapStateToProps = state => ({ speakMode: state.user.speakMode });

export default connect(mapStateToProps)(HomeScreenWithNavigation);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  menu: {
    position: "absolute",
    left: 15,
    top: 15
  },
  title: {
    marginTop: 100
  },
  image: {
    width: 161,
    height: 170,
    marginVertical: 15,
    alignSelf: "center"
  },
  developmentModeText: {
    marginBottom: 20,
    color: "rgba(0,0,0,0.4)",
    fontSize: 14,
    lineHeight: 19,
    textAlign: "center"
  },
  contentContainer: {
    paddingTop: 30
  },
  welcomeContainer: {
    alignItems: "center",
    marginTop: 10,
    marginBottom: 20
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: "contain",
    marginTop: 3,
    marginLeft: -10
  },
  getStartedContainer: {
    alignItems: "center",
    marginHorizontal: 50
  },
  homeScreenFilename: {
    marginVertical: 7
  },
  codeHighlightText: {
    color: "rgba(96,100,109, 0.8)"
  },
  codeHighlightContainer: {
    backgroundColor: "rgba(0,0,0,0.05)",
    borderRadius: 3,
    paddingHorizontal: 4
  },
  getStartedText: {
    fontSize: 17,
    color: "rgba(96,100,109, 1)",
    lineHeight: 24,
    textAlign: "center"
  },
  tabBarInfoContainer: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: "black",
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3
      },
      android: {
        elevation: 20
      }
    }),
    alignItems: "center",
    backgroundColor: "#fbfbfb",
    paddingVertical: 20
  },
  tabBarInfoText: {
    fontSize: 17,
    color: "rgba(96,100,109, 1)",
    textAlign: "center"
  },
  navigationFilename: {
    marginTop: 5
  },
  helpContainer: {
    marginTop: 15,
    alignItems: "center"
  },
  helpLink: {
    paddingVertical: 15
  },
  helpLinkText: {
    fontSize: 14,
    color: "#2e78b7"
  }
});
