import React from "react";
import { connect } from "react-redux";
import { Icon } from "react-native-elements";
import { Text, StyleSheet, View, Image, ScrollView } from "react-native";
import { withNavigationFocus } from "react-navigation";
import Tts from 'react-native-tts';
import LinearGradient from "react-native-linear-gradient";
import Ingredients from "../components/Ingredients";

const LIGHT_COLOR = "#f093fb";
const DARK_COLOR = "#f5576c";

class LastMealScreen extends React.PureComponent {
  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    if (this.props.isFocused && this.props.speakMode) this._speak();
  }  

  componentDidUpdate(prevProps) {
    if (!prevProps.isFocused && this.props.isFocused && this.props.speakMode) this._speak();
  }  

  render() {
    let meal = this.props.navigation.getParam("meal", null);
    let picture = this.props.navigation.getParam("photo", null);

    if (!meal && this.props.meals.length !== 0) {
      meal = this.props.meals[0];
      picture = `data:image/jpg;base64,${meal.formattedImage}`;
    }

    return (
      <View style={styles.container}>
        <LinearGradient
          colors={[LIGHT_COLOR, DARK_COLOR]}
          style={styles.linearGradient}
        >
          <View style={styles.menu}>
            <Icon
              name="bars"
              type="font-awesome"
              color="#fff"
              onPress={this._onMenuClick}
              underlayColor={"transparent"}
            />
            <Text style={styles.headerText}>Last Meal</Text>
          </View>
        </LinearGradient>
        {meal && picture ? (
          <View style={styles.contentContainer}>
            <Text style={styles.details}>
              {new Date(meal.time).toDateString()}
            </Text>
            <Image
              style={styles.image}
              resizeMode="cover"
              source={{ uri: picture }}
            />
            <View style={styles.content}>
              <Text style={styles.mealName}>{meal.name}</Text>
              <ScrollView>
                <Ingredients food={meal.food} />
              </ScrollView>
            </View>
          </View>
        ) : (
          <View style={styles.message}>
            <Text style={styles.messageText}>
              No photo has been taking yet...
            </Text>
            <Text style={styles.takePhoto} onPress={this._takePhoto}>
              Take your first one now!
            </Text>
          </View>
        )}
        <View style={styles.addBtn}>
          <Icon
            reverse
            name="add"
            color={DARK_COLOR}
            onPress={this._takePhoto}
            underlayColor={"transparent"}
          />
        </View>
      </View>
    );
  }

  _onMenuClick = () => this.props.navigation.openDrawer();

  _takePhoto = () => this.props.navigation.navigate("Photo");

  _speak() {
    let meal = this.props.navigation.getParam("meal", null);

    if (!meal && this.props.meals.length !== 0) {
      meal = this.props.meals[0];
    }

    if (meal) {
      Tts.speak(`Your meal is ${meal.name}`);
      Tts.speak(`The ingredients are: `);
      meal.food.map(food =>  Tts.speak(`${food.name}`));
    }
    else {
      Tts.speak("No photo has been taking yet...");
    }
  }
}

const LastMealScreenWithNavigation = withNavigationFocus(LastMealScreen);

const mapStateToProps = state => ({ meals: state.user.meals, speakMode: state.user.speakMode });

export default connect(mapStateToProps)(LastMealScreenWithNavigation);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column"
  },
  contentContainer: {
    flex: 1,
    backgroundColor: "#e0e0e0"
  },
  linearGradient: {
    width: "100%",
    flexDirection: "column",
    borderBottomColor: "#e0e0e0",
    borderBottomWidth: 2,
    paddingVertical: 10
  },
  headerText: {
    color: "#ffffff",
    marginLeft: 15,
    fontSize: 18,
    fontFamily: "Comfortaa-Bold"
  },
  menu: {
    margin: 15,
    marginBottom: 10,
    flexDirection: "row",
    alignItems: "center"
  },
  image: {
    width: 220,
    height: 220,
    marginTop: 5,
    borderRadius: 110,
    alignSelf: "center",
    borderColor: "#a30808",
    borderWidth: 3,
    zIndex: 1
  },
  content: {
    marginTop: -110,
    borderColor: "#bdbdbd",
    borderWidth: 2,
    margin: 20,
    backgroundColor: "#fafafa"
  },
  mealName: {
    fontSize: 26,
    paddingTop: 110,
    padding: 20,
    color: "#fafafa",
    backgroundColor: DARK_COLOR,
    textAlign: "center",
    fontFamily: "Comfortaa-Bold",
    lineHeight: 40
  },
  details: {
    fontSize: 18,
    margin: 10,
    textAlign: "center",
    color: DARK_COLOR,
    fontFamily: "Comfortaa-Bold"
  },
  message: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  messageText: {
    fontFamily: "Comfortaa-Bold"
  },
  takePhoto: {
    marginTop: 20,
    alignSelf: "center",
    fontSize: 18,
    fontFamily: "Comfortaa-Bold"
  },
  addBtn: {
    position: "absolute",
    bottom: 5,
    right: 5
  }
});
