import React from "react";
import { connect } from "react-redux";
import { Avatar, Button, Icon, Input } from "react-native-elements";
import {
  Text,
  StyleSheet,
  View,
  ScrollView,
  Image,
  Switch
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { bindActionCreators } from "redux";
import AsyncStorage from "@react-native-community/async-storage";
import FBLoginButton from "../components/FBLoginButton";
import { withNavigationFocus, withNavigation } from "react-navigation";
import Tts from 'react-native-tts';
import { Api } from "../constants/api";
import { setUser, setSpeakMode } from "../reducers/UserActions";
import FlashMessage, { showMessage } from "react-native-flash-message";
import ImagePicker from "react-native-image-picker";

const LIGHT_COLOR = "#f093fb";
const DARK_COLOR = "#f5576c";

class AccountScreen extends React.PureComponent {
  static navigationOptions = {
    header: null
  };

  state = {
    newUsername: this.props.user.username,
    oldPassword: "",
    newPassword: "",
    isFacebookUser: false
  };

  async componentWillMount() {
    const fbToken = await AsyncStorage.getItem("facebookToken");

    this.setState({ isFacebookUser: !!fbToken });
  }

  componentDidMount() {
    if (this.props.isFocused && this.props.user.speakMode) this._speak();
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.isFocused && this.props.isFocused && this.props.user.speakMode) this._speak();
  }

  render() {
    const { username, email, formattedImage, meals, fbImage, speakMode } = this.props.user;

    return (
      <View style={styles.container}>
        <LinearGradient
          colors={[LIGHT_COLOR, DARK_COLOR]}
          style={styles.linearGradient}
        >
          <View style={styles.menu}>
            <Icon
              name="bars"
              type="font-awesome"
              color="#fff"
              onPress={this._onMenuClick}
              underlayColor={"transparent"}
            />
          </View>
          <View style={styles.imageContainer}>
            <Avatar
              rounded
              size="xlarge"
              showEditButton={!this.state.isFacebookUser}
              activeOpacity={0.7}
              source={{
                uri: this.state.isFacebookUser
                  ? fbImage
                  : `data:image/jpg;base64,${formattedImage}`
              }}
              onEditPress={this.onEditImage}
            />
          </View>
          <Text style={styles.userName}>{username}</Text>
          <Text style={styles.userEmail}>{email}</Text>
        </LinearGradient>
        <ScrollView contentContainerStyle={styles.contentContainer}>
          <Text style={styles.meals}>
            {meals.length === 0
              ? "No meals yet!"
              : `You have ${meals.length} meals!`}
          </Text>
          <View />
          {this.state.isFacebookUser ? (
            <View style={styles.fbContainer}>
              <Text style={styles.fbText}>You are logged in with Facebook</Text>
              <FBLoginButton
                style={styles.fbButton}
                onLogout={this._onFacebookLogout}
              />
            </View>
          ) : (
            <View style={styles.inputs}>
              <Input
                inputContainerStyle={styles.inputContainer}
                inputStyle={styles.input}
                placeholder="New User Name"
                leftIcon={
                  <Icon
                    name="user-circle"
                    size={24}
                    color={LIGHT_COLOR}
                    type="font-awesome"
                  />
                }
                onChangeText={newUsername => this.setState({ newUsername })}
                value={this.state.newUsername}
              />
              <Input
                inputContainerStyle={styles.inputContainer}
                inputStyle={styles.input}
                placeholder="Old Password"
                secureTextEntry={true}
                leftIcon={
                  <Icon
                    name="lock"
                    size={24}
                    color={LIGHT_COLOR}
                    type="font-awesome"
                  />
                }
                onChangeText={oldPassword => this.setState({ oldPassword })}
                value={this.state.oldPassword}
              />
              <Input
                inputContainerStyle={styles.inputContainer}
                inputStyle={styles.input}
                placeholder="New Password"
                secureTextEntry={true}
                leftIcon={
                  <Icon
                    name="lock"
                    size={24}
                    color={LIGHT_COLOR}
                    type="font-awesome"
                  />
                }
                onChangeText={newPassword => this.setState({ newPassword })}
                value={this.state.newPassword}
              />
              <Button
                raised
                title="SAVE"
                type="solid"
                buttonStyle={styles.saveBtn}
                containerStyle={styles.saveBtnContainer}
                onPress={this.onSubmit}
              />
            </View>
          )}
          <View style={styles.switch}>
            <Icon
              name="volume-up"
              size={24}
              color={LIGHT_COLOR}
              type="font-awesome"
            />
            <Text style={styles.switchText}>
              {speakMode ? "Speak mode is ON" : "Speak mode is OFF"}
            </Text>
            <Switch
              style={{ marginTop: 10 }}
              onValueChange={this._toggleSpeakMode}
              value={speakMode}
              thumbColor={DARK_COLOR}
              trackColor={{ true: LIGHT_COLOR }}
            />
          </View>
        </ScrollView>
        <View style={styles.addBtn}>
          <Icon
            reverse
            name="add"
            color={DARK_COLOR}
            onPress={this._takePhoto}
            underlayColor={"transparent"}
          />
        </View>
        {meals.length >= 3 && (
          <ScrollView horizontal style={styles.gallery}>
            {meals.map(_ => (
              <Image
                key={_._id}
                source={{ uri: `data:image/jpg;base64,${_.formattedImage}` }}
                resizeMode="cover"
                style={styles.image}
              />
            ))}
          </ScrollView>
        )}
        <FlashMessage duration={1000} ref="saveMessage" />
      </View>
    );
  }

  onEditImage = () => {
    ImagePicker.launchImageLibrary({}, async response => {
      if (response.didCancel) {
        return;
      }

      const { path, type, fileName, uri } = response;

      const form = new FormData();

      form.append("image", { uri, name: fileName, type });
      try {
        const user = await this.uploadUserImage(form);

        this.props.setUser(user);

        showSuccessMessage();
      } catch (err) {
        alert(err);
      }
    });
  };

  async uploadUserImage(formData) {
    return await Api.upload("/api/user/me/image", formData);
  }

  onSubmit = async () => {
    const { newUsername, oldPassword, newPassword } = this.state;

    if (oldPassword && newPassword.length < 2) {
      alert("Password name must have at least 2 characters");
      return;
    }

    if (
      newUsername.length > 0 &&
      (newUsername.length < 2 || newUsername.length > 8)
    ) {
      alert(
        "User name must have at least 2 characters and max of 8 characters"
      );
      return;
    }

    if (!newUsername && !oldPassword && !newPassword) {
      alert("Please enter username or password to edit");
      return;
    }

    try {
      const user = await Api.put("/api/user/me", {
        newUsername,
        oldPassword,
        newPassword
      });

      this.props.setUser(user);

      this.clearPassword();

      showSuccessMessage();
    } catch (e) {
      alert(
        "Sorry, there was an error updating your account, please try again later"
      );
    }
  };

  clearPassword = () => {
    this.setState({ oldPassword: "", newPassword: "" });
  };

  _onMenuClick = () => this.props.navigation.openDrawer();

  _takePhoto = () => this.props.navigation.navigate("Photo");

  _onFacebookLogout = async () => {
    await AsyncStorage.clear();
    
    if (this.props.user.speakMode) Tts.speak(`You logged out`);
    
    this.props.navigation.navigate("Auth");
  };

  _toggleSpeakMode = () => {
    const { setSpeakMode, user: { speakMode }} = this.props; 
    setSpeakMode(!speakMode);
    Tts.speak(`Speak mode is ${speakMode ? "off" : "on"}`);
  }

  _speak = () => Tts.speak(`You arrived to account screen`);
}

const showSuccessMessage = () =>
  showMessage({
    message: "Save Success!",
    type: "success"
  });

const AccountScreenWithNavigation = withNavigationFocus(AccountScreen)

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setUser, setSpeakMode }, dispatch);

const mapStateToProps = state => ({ user: state.user });

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountScreenWithNavigation);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column"
  },
  contentContainer: {
    flex: 1,
    backgroundColor: "#e0e0e0"
  },
  inputs: {
    width: "100%",
    alignItems: "center"
  },
  linearGradient: {
    width: "100%",
    flexDirection: "column",
    alignItems: "center",
    borderBottomColor: "#e0e0e0",
    borderBottomWidth: 2,
    paddingVertical: 10
  },
  menu: {
    position: "absolute",
    top: 10,
    left: 10
  },
  imageContainer: {
    marginVertical: 15
  },
  userName: {
    color: "#ffffff",
    fontSize: 20,
    fontFamily: "Comfortaa-Bold"
  },
  userEmail: {
    color: "#ffffff",
    fontSize: 18,
    fontFamily: "Comfortaa-Bold"
  },
  meals: {
    color: DARK_COLOR,
    fontSize: 18,
    fontFamily: "Comfortaa-Bold",
    marginVertical: 10
  },
  contentContainer: {
    alignItems: "center",
    justifyContent: "center"
  },
  inputContainer: {
    margin: 10,
    borderWidth: 1,
    borderColor: "#e0e0e0",
    borderRadius: 10
  },
  input: {
    paddingLeft: 10,
    fontFamily: "Comfortaa-Bold"
  },
  saveBtnContainer: {
    margin: 10,
    width: 100
  },
  saveBtn: {
    width: 100,
    backgroundColor: LIGHT_COLOR
  },
  fbContainer: {
    alignItems: "center"
  },
  fbText: {
    fontFamily: "Comfortaa-Bold"
  },
  fbButton: {
    height: 30,
    width: 250,
    marginVertical: 10,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center"
  },
  addBtn: {
    position: "absolute",
    bottom: 5,
    right: 5,
    zIndex: 2
  },
  image: {
    width: 140,
    height: 115,
    borderWidth: 2,
    borderColor: LIGHT_COLOR,
    marginHorizontal: 10
  },
  gallery: {
    position: "absolute",
    flexDirection: "row",
    bottom: 20,
    paddingVertical: 10
  },
  switch: {
    marginTop: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  switchText: {
    marginHorizontal: 10,
    fontSize: 16,
    fontFamily: "Comfortaa-Bold"
  }
});
