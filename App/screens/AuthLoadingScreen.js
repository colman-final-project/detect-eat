import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import { setUser } from "../reducers/UserActions";
import getFacebookUser from "../utility/getFacebookUser";

import { ActivityIndicator, StatusBar, StyleSheet, View } from "react-native";

import AsyncStorage from "@react-native-community/async-storage";
import { Api } from "../constants/api";

class AuthLoadingScreen extends React.Component {
  constructor() {
    super();
    this._bootstrapAsync();
  }

  _bootstrapAsync = async () => {
    const fbToken = await AsyncStorage.getItem("facebookToken");
    const localToken = await AsyncStorage.getItem("localToken");

    if (!fbToken && !localToken) {
      this.props.navigation.navigate("Auth");
      return;
    }

    if (fbToken) {
      try {
        const user = await getFacebookUser(fbToken);
        await this._setUser(user);
      } catch (err) {
        alert("Error getting data from facebook");
        this.props.navigation.navigate("Auth");
      }
    } else if (localToken)
      Api.me()
        .then(async ({ user }) => await this._setUser(user))
        .catch(error => {
          if (error.message === 401) {
            this.props.navigation.navigate("Auth");
            return;
          }

          return alert("Error getting user info");
        });
  };

  async _setUser(user) {
    user.meals = await Api.get(`/api/meal/${user.email}`);
    this.props.setUser(user);
    this.props.navigation.navigate("App");
  }

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setUser }, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(AuthLoadingScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});
