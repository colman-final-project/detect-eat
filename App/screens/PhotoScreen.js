import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Dimensions, StyleSheet, TouchableOpacity, View, ActivityIndicator, Text } from 'react-native';
import Tts from 'react-native-tts';
import { RNCamera } from 'react-native-camera';
import { withNavigationFocus } from "react-navigation";

import { Api } from '../constants/api';
import path from 'path';
import ImagePicker from 'react-native-image-picker';
import { Icon } from 'react-native-elements';
import { addMeal } from '../reducers/UserActions';

const LOADING_MESSAGE = "Please wait, it may take a few seconds...";
const UNABLE_TO_IDENTIFY_MESSAGE = "We were unable to identify the meal, please take another shot";

class PhotoScreen extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);

    this.state = {
      camEnabled: true,
      isLoading: false
    };
  }

  componentDidMount() {
    const { navigation } = this.props;

    navigation.addListener('willFocus', () =>
      this.setState({ camEnabled: true })
    );

    navigation.addListener('willBlur', () =>
      this.setState({ camEnabled: false })
    );

    if (this.props.isFocused && this.props.user.speakMode) this._speak();
  }

  componentDidUpdate(prevProps) {
    if (!prevProps.isFocused && this.props.isFocused && this.props.user.speakMode) this._speak();
  }

  render() {
    return !this.state.camEnabled ? (
      <View/>
    ) : (
      <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.auto}
          captureAudio={false}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel'
          }}
        />
        <View style={styles.captureContainer}>
          <TouchableOpacity
            onPress={this._takePicture}
            style={styles.capture}
          />
        </View>
        <View style={styles.fromGallery}>
          <Icon
            name="image"
            type="font-awesome"
            color="#fff"
            onPress={this._getFromGallery}
            underlayColor={'transparent'}
          />
        </View>
        {this.state.isLoading && (
          <View style={styles.loadingContainer}>
            <Text style={styles.loadingText}>{LOADING_MESSAGE}</Text>
            <ActivityIndicator size="large"/>
          </View>
        )}
      </View>
    );
  }

  _speak = () => Tts.speak("You arrived to new meal screen");

  _takePicture = async () => {
    if (this.camera) {
      const options = { quality: 0.5, fixOrientation: true };
      const data = await this.camera.takePictureAsync(options);

      try {
        const meal = await this._addMeal(createImageData(data));

        this.props.navigation.navigate('LastMeal', { photo: data.uri, meal });

      } catch (err) {
        this.setState({ isLoading: false });

        if (err.message === '404') {
          if (this.props.user.speakMode) Tts.speak(UNABLE_TO_IDENTIFY_MESSAGE);
          else alertNoMeal();
        } else {
          alert('An error has occurred');
        }
      }
    }
  };

  _getFromGallery = () => {
    ImagePicker.launchImageLibrary({}, async response => {
      if (response.didCancel) return;
      const { type, fileName, uri } = response;

      const form = new FormData();
      form.append('image', { uri, name: fileName, type });
      try {
        const meal = await this._addMeal(form);

        this.props.navigation.navigate('LastMeal', { photo: uri, meal });

      } catch (err) {
        this.setState({ isLoading: false });

        if (err.message === '404') {
          if (this.props.user.speakMode) Tts.speak(UNABLE_TO_IDENTIFY_MESSAGE);
          else alertNoMeal();
        } else {
          alert('An error has occurred');
        }
      }
    });
  };

  async _addMeal(formData) {
    this.setState({ isLoading: true });
    if (this.props.user.speakMode) Tts.speak(LOADING_MESSAGE);

    const meal = await Api.upload(
      `/api/meal/${this.props.user.email}/detect`,
      formData
    );

    this.setState({ isLoading: false });

    if (!meal) {
      throw Error(404);
    }

    this.props.addMeal(meal);

    return meal;
  }
}

const alertNoMeal = () => alert(UNABLE_TO_IDENTIFY_MESSAGE);

const createImageData = ({ uri }) => {
  const form = new FormData();

  const name = path.basename(uri);
  const type = `image/${path
    .extname(uri)
    .split('.')
    .pop()}`;

  form.append('image', { uri, name, type });

  return form;
};

const PhotoScreenWithNavigation = withNavigationFocus(PhotoScreen);

const mapStateToProps = state => ({ user: state.user });
const mapDispatchToProps = dispatch =>
  bindActionCreators({ addMeal }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PhotoScreenWithNavigation);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black'
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  fromGallery: {
    position: 'absolute',
    bottom: 30,
    left: 30
  },
  captureContainer: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0
  },
  capture: {
    alignSelf: 'center',
    height: 60,
    width: 60,
    backgroundColor: '#fff',
    borderRadius: 30,
    margin: 20
  },
  loadingContainer: {
    position: 'absolute',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    backgroundColor: 'rgba(0,0,0,0.7)',
    alignItems: 'center',
    justifyContent: 'center'
  },
  loadingText: {
    color: '#ffffff',
    fontSize: 18,
    fontFamily: 'Comfortaa-Bold',
    marginHorizontal: 50,
    marginBottom: 15,
    textAlign: 'center',
    lineHeight: 30
  }
});
