import 'dotenv/config';
import express from 'express';
import helmet from 'helmet';
import { urlencoded, json } from 'body-parser';
import morgan from 'morgan';
import compression from 'compression';
import methodOverride from 'method-override';
import inProduction from 'in-production';
import passport from 'passport';
import errorHandler from 'express-error-log-handler';

import './config/mongoose';

import applyRoutes from './routes';

export default () => {
  const app = express();

  app.use(helmet());
  app.use(urlencoded({ extended: false }));
  app.use(json());
  app.use(methodOverride());
  app.use(compression());
  app.use(passport.initialize());

  if (!inProduction) {
    app.use(morgan('dev'));
  }

  applyRoutes(app);

  app.use(errorHandler(({stack, message}) => {
    console.log(stack);
    console.log(message);
  }));
  return app;
};
