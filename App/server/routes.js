import { route as userRoute, router as userRouter } from './api/user';
import { route as mealRoute, router as mealRouter} from './api/meal';
import { route as authRoute, router as authRouter } from './auth';

export default app => {
  app.use(`/api/${userRoute}`, userRouter);
  app.use(`/api/${mealRoute}`, mealRouter);
  app.use(`/${authRoute}`, authRouter);
};
