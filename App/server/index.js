import port from './config/port';
import argv from './config/argv';
import logger from './logger';
import createApp from './app';

const app = createApp();

const host = argv.host || process.env.HOST || null;

export let server;

export const started = new Promise(resolve => {
  server = app.listen(port, host, err => {
    if (err) {
      logger.error(err.message);
    }

    logger.appStarted(port, host);

    resolve();
  });
});

export const close = () => {
  console.log('Server closes');
  server.close();
};
