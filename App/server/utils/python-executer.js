import { PythonShell } from 'python-shell';
import path from 'path';
import pify from 'pify';

PythonShell.defaultOptions = { scriptPath: '/darknet/' };

const script = 'PredictOnServer.py';

export function executeDetection(image) {
  return pify(PythonShell.run)(script,
    {
      pythonOptions: ['-u'],
      args: [image]
    })
    .then((results) => {
        console.log('results: ' + results);
        return results;
      }
    );
}
