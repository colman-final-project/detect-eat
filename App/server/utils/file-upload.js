import multer from 'multer';
import path from 'path';
import uuid from 'uuid/v1';
import {promises as fsp} from 'fs';

export default multer({
  storage: multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, path.resolve(process.env.FILE_STORAGE));
    },
    filename: (req, file, cb) => {
      cb(null, `${uuid()}.${file.originalname.split('.').pop()}`);
    }
  })
});

export async function remove(filename) {
  return fsp.unlink(path.resolve(process.env.FILE_STORAGE, filename));
}

export const defaultUserImage = 'default.jpeg';
