import passport from "passport";

import { signToken } from "../auth.service";
import User from "../../api/user/user.model";
import createError from 'http-errors';

export function register(req, res, next) {
  return User.register(
    new User({ email: req.body.email, username: req.body.username }),
    req.body.password,
    (err, user) => {
      if (err) {
        return next(err);
      }

      req.login(user, { session: false }, err => {
        if (err) {
          return next(err);
        }

        res.json({ token: signToken(user), user });
      });
    }
  );
}

export function login(req, res, next) {
  return passport.authenticate(
    "local",
    { session: false },
    (err, user) => {
      if (err) {
        return next(err);
      }
      if (!user) {
        next(createError(404));
        return;
      }

      res.json({ token: signToken(user), user });
    }
  )(req, res, next);
}
