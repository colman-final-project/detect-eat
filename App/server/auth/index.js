import { Router } from 'express';

import { route as localRoute, router as localRouter } from './local';
import './passport';

const route = 'auth';

const router = new Router();

router.use(`/${localRoute}`, localRouter);

export { route, router };
