import passport from 'passport';

import User from '../api/user/user.model';

passport.use(User.createStrategy());

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// passport.use('jwt', new JwtStrategy());
