import { Schema, model } from 'mongoose';
import passportPlugin from 'passport-local-mongoose';
import fs from 'fs';
import path from 'path';
import { defaultUserImage } from '../../utils/file-upload';

const schema = new Schema({
  email: String,
  image: String,
  predictions: [{ type: Schema.Types.ObjectId, ref: 'Meal' }]
}, { toJSON: { virtuals: true } });

schema.methods.toBasic = function() {
  const user = Object.assign({}, this._doc);
  delete user.salt;
  delete user.hash;
  return user;
};

schema.virtual('formattedImage').get(function() {
  let image = this.image;

  if (!image || !fs.existsSync(path.resolve(process.env.FILE_STORAGE, image))) {
    image = defaultUserImage
  }

  const bitmap = fs.readFileSync(path.resolve(process.env.FILE_STORAGE, image));

  return new Buffer(bitmap).toString('base64');
});

schema.plugin(passportPlugin);

export default model('User', schema);
