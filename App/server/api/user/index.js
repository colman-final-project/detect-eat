import { AsyncRouter } from 'express-async-router';
import * as controller from './user.controller';
import { isAuthenticated } from '../../auth/auth.service';
import upload from '../../utils/file-upload';

const route = 'user';

const router = new AsyncRouter();

router.get('/me', isAuthenticated, controller.me);
router.put('/me', isAuthenticated, controller.update);
router.post('/me/image', isAuthenticated, upload.single('image'), controller.updateImage);

export { route, router };
