import { defaultUserImage } from '../../utils/file-upload';

export function me({ user }, res) {
  if (!user.image) {
    user.image = defaultUserImage;
  }

  res.json({ user });
}

export async function update({ user, body: { oldPassword, newPassword, newUsername } }, res, next) {
  try {

    if (newPassword) {
      await user.changePassword(oldPassword, newPassword);
    }

    if (newUsername && user.username !== newUsername) {
      user.username = newUsername;

      await user.save();
    }

    res.json(user);

  } catch (e) {
    next(e);
  }
}

export async function updateImage({ user, file }, res) {
  user.image = file.filename;

  await user.save();

  res.json(user);
}
