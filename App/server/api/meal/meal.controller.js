import { executeDetection } from '../../utils/python-executer';
import Meal from './meal.model';
import { Food } from '../food/food.model';
import {remove} from '../../utils/file-upload';

import createError from 'http-errors';

export async function detect(req, res, next) {
  let mealsNames;

  try {
    mealsNames = await executeDetection(req.file.filename);
  } catch (err) {
    next(err);
  }

  if (!mealsNames) {
    next(createError(404, 'Items were not found'));
  }

  const meal = new Meal({
    name: mealsNames.join(),
    image: req.file.filename,
    user: req.params.email,
    food: await Food.find().where('name').in(mealsNames).exec()
  });

  await meal.save();

  return meal;
}

export async function getByUser(req, res, next) {
  try {
    res.json(await Meal.find({ user: req.params.email }).populate('food').sort('-time').exec());
  } catch (e) {
    next(e);
  }
}

export async function deleteOne(req, res, next) {
  try {
    const meal = await Meal.where().findOneAndDelete({ _id: req.params.id }).exec();

    res.json(meal);

    await remove(meal.image);

  } catch (e) {
    next(e);
  }
}
