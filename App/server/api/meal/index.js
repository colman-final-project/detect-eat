import { AsyncRouter } from 'express-async-router';

import { isAuthenticated } from '../../auth/auth.service';
import upload from '../../utils/file-upload';
import * as controller from './meal.controller';

const route = 'meal';

const router = new AsyncRouter();

router.get('/:email', controller.getByUser);
router.post('/:email/detect', upload.single('image'), controller.detect);
router.delete('/:id', controller.deleteOne);
export { route, router };
