import { Schema, model } from 'mongoose';
import fs from 'fs';
import path from 'path';

const schema = new Schema({
  name: String,
  image: String,
  user: String,
  time: { type: Date, default: Date.now },
  food: [{ type: Schema.Types.ObjectId, ref: 'Food' }]
}, { toJSON: { virtuals: true } });

schema.virtual('formattedImage').get(function() {
  const bitmap = fs.readFileSync(path.resolve(process.env.FILE_STORAGE, this.image));

  return new Buffer(bitmap).toString('base64');
});

export default model('Meal', schema);
