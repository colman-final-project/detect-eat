import { Schema, model } from 'mongoose';

const detailSchema = new Schema({
  name: String,
  value: Number
}, { _id: false });

const foodSchema = new Schema({
  contents: {detailSchema}
});

const Food = model('Food', foodSchema);
const Detail = model('Detail', detailSchema);

export { Food , Detail };
