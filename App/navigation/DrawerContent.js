import React from "react";
import {
  View,
  ScrollView,
  ImageBackground,
  Text,
  StyleSheet
} from "react-native";
import Tts from 'react-native-tts';
import { Button, Avatar } from "react-native-elements";
import { DrawerItems } from "react-navigation";
import { LoginManager } from "react-native-fbsdk";
import AsyncStorage from "@react-native-community/async-storage";
import Colors from "../constants/Colors";

export default class DrawerContent extends React.Component {
  render() {
    const { user } = this.props.screenProps;

    return (
      <View style={styles.container}>
        <ScrollView>
          <ImageBackground
            source={require("../assets/images/navigation.jpg")}
            style={styles.image}
          >
            <View style={styles.header}>
              <Avatar
                rounded
                size="large"
                activeOpacity={0.7}
                source={{
                  uri: user.fbImage || `data:image/jpg;base64,${user.formattedImage}`
                }}
              />
              <View style={{ paddingLeft: 15 }}>
                <Text style={styles.text}>Hello,</Text>
                <Text style={styles.text}>{user && user.username}</Text>
              </View>
            </View>
          </ImageBackground>
          <DrawerItems {...this.props} />
        </ScrollView>
        <Button
          type="clear"
          buttonStyle={styles.logout}
          titleStyle={styles.logoutText}
          title="Logout"
          onPress={async () => {
            await AsyncStorage.clear();
            this.props.navigation.navigate("Auth");
            LoginManager.logOut();
            if (user.speakMode) Tts.speak(`You logged out`);
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: "100%"
  },
  image: {},
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    padding: 15
  },
  text: {
    color: "#efebe9",
    fontSize: 17,
    fontFamily: "Comfortaa-Bold"
  },
  profile: {
    width: 60,
    height: 60,
    borderRadius: 30,
    borderWidth: 1,
    borderColor: "#efebe9"
  },
  logout: {
    alignSelf: "center",
    marginBottom: 10
  },
  logoutText: {
    color: Colors.tintColor,
    fontFamily: "Comfortaa-Bold"
  }
});
