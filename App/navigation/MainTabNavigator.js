import React from "react";
import {
  createStackNavigator,
  createDrawerNavigator,
} from "react-navigation";

import SideBarIcon from "../components/SideBarIcon";
import HomeScreen from "../screens/HomeScreen";
import PhotoScreen from "../screens/PhotoScreen";
import LastMealScreen from "../screens/LastMealScreen";
import HistoryScreen from '../screens/HistoryScreen';
import AccountScreen from '../screens/AccountScreen';

import DrawerContent from "./DrawerContent";
import Colors from '../constants/Colors';

const HomeStack = createStackNavigator({ Home: HomeScreen });

HomeStack.navigationOptions = {
  drawerLabel: "Home",
  drawerIcon: ({ focused }) => (
    <SideBarIcon focused={focused} name="home" type="font-awesome" />
  )
};

const PhotoStack = createStackNavigator({ Photo: PhotoScreen });

PhotoStack.navigationOptions = {
  drawerLabel: "New Meal",
  drawerIcon: ({ focused }) => (
    <SideBarIcon focused={focused} name="camera" type="font-awesome" />
  )
};

const AccountStack = createStackNavigator({ Account: AccountScreen });

AccountStack.navigationOptions = {
  drawerLabel: "Account",
  drawerIcon: ({ focused }) => (
    <SideBarIcon focused={focused} name="user-circle" type="font-awesome" />
  )
};

const LastMealStack = createStackNavigator({ LastMeal: LastMealScreen });

LastMealStack.navigationOptions = {
  drawerLabel: "Last Meal",
  drawerIcon: ({ focused }) => (
    <SideBarIcon focused={focused} name="restaurant" />
  )
};

const RecentStack = createStackNavigator({ Recent: HistoryScreen });

RecentStack.navigationOptions = {
  drawerLabel: "Recent Meals",
  drawerIcon: ({ focused }) => (
    <SideBarIcon focused={focused} name="access-time" />
  )
};

// const HelpStack = createStackNavigator({ Help: LastMealScreen });

// HelpStack.navigationOptions = {
//   drawerLabel: "Help",
//   drawerIcon: ({ focused }) => (
//     <SideBarIcon focused={focused} name="question" type="font-awesome" />
//   )
// };

export default createDrawerNavigator(
  {
    HomeStack,
    PhotoStack,
    AccountStack,
    LastMealStack,
    RecentStack,
    // HelpStack
  },
  {
    drawerWidth: 250,
    contentOptions: {
      activeTintColor: Colors.tintColor,
      labelStyle: {
        fontWeight: "normal",
        fontFamily: "Comfortaa-Bold"
      }
    },
    contentComponent: props => <DrawerContent {...props} />
  }
);
