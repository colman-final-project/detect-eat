import React from "react";
import { connect } from "react-redux";

import AppNavigator from "./AppNavigator";

class NavigatorContainer extends React.Component {
  render() {
    return <AppNavigator screenProps={{ user: this.props.user }} />;
  }
}

const mapStateToProps = state => ({ user: state.user });

export default connect(mapStateToProps)(NavigatorContainer);
