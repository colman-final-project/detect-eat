import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import { AccordionList } from "accordion-collapse-react-native";

export default class Ingredients extends React.Component {
  render() {
    return this.props.food.length === 1 ? (
      this._body(this.props.food[0])
    ) : (
      <AccordionList
        list={this.props.food}
        header={this._header}
        body={this._body}
      />
    );
  }

  _header = ({ name, contents }) => {
    return (
      <View style={styles.accordionHeader}>
        <Text style={styles.details}>{name}</Text>
        <Text style={styles.details}>{contents.calories} klg</Text>
      </View>
    );
  };

  _body = ({ contents }) => {
    return (
      <View style={styles.accordionContent}>
        <View style={styles.ingredient}>
          <Image
            resizeMode="cover"
            source={require("../assets/images/sprinkles.png")}
            style={styles.ingredientIcon}
          />
          <View style={styles.ingredientContent}>
            <Text style={styles.ingredientName}>Calories</Text>
            <Text style={styles.ingredientValue}>
              {contents.calories} klg
            </Text>
          </View>
        </View>
        <View style={styles.ingredient}>
          <Image
            resizeMode="cover"
            source={require("../assets/images/carbs.png")}
            style={styles.ingredientIcon}
          />
          <View style={styles.ingredientContent}>
            <Text style={styles.ingredientName}>Carbohydrates</Text>
            <Text style={styles.ingredientValue}>
              {contents.carbs} g
            </Text>
          </View>
        </View>
        <View style={styles.ingredient}>
          <Image
            resizeMode="cover"
            source={require("../assets/images/pizza-slice.png")}
            style={styles.ingredientIcon}
          />
          <View style={styles.ingredientContent}>
            <Text style={styles.ingredientName}>Fat</Text>
            <Text style={styles.ingredientValue}>
              {contents.fat} g
            </Text>
          </View>
        </View>
        <View style={styles.ingredient}>
          <Image
            resizeMode="cover"
            source={require("../assets/images/fried-eggs.png")}
            style={styles.ingredientIcon}
          />
          <View style={styles.ingredientContent}>
            <Text style={styles.ingredientName}>Proteins</Text>
            <Text style={styles.ingredientValue}>
              {contents.protein} g
            </Text>
          </View>
        </View>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  accordionHeader: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#fafafa",
    borderBottomColor: "#bdbdbd",
    borderBottomWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 10
  },
  details: {
    fontSize: 14,
    fontFamily: "Comfortaa-Bold"
  },
  accordionContent: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 5,
    paddingBottom: 5,
    borderBottomColor: "#e0e0e0",
    borderBottomWidth: 1
  },
  ingredient: {
    flexDirection: "column",
    alignItems: "center",
    marginTop: 10
  },
  ingredientIcon: {
    height: 20,
    width: 20,
    marginRight: 5
  },
  ingredientContent: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: 10
  },
  ingredientName: {
    fontFamily: "Comfortaa-Bold"
  },
  ingredientValue: {
    paddingRight: 5,
    fontSize: 14,
    color: "#f093fb",
    fontFamily: "Comfortaa-Bold"
  }
});
