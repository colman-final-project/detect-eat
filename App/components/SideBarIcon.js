import React from 'react';
import { Icon } from 'react-native-elements';

import Colors from '../constants/Colors';

export default class SideBarIcon extends React.Component {
  render() {
    return (
      <Icon
        name={this.props.name}
        type={this.props.type}
        color={this.props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
      />
    );
  }
}