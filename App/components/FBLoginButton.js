import React, { Component } from "react";
import { View } from "react-native";
import { LoginButton, AccessToken } from "react-native-fbsdk";

export default class FBLoginButton extends Component {
  render() {
    return (
      <View>
        <LoginButton
          style={this.props.style}
          readPermissions={["public_profile email"]}
          onLoginFinished={(error, result) => {
            if (error) {
              alert("Login failed with error: " + error.message);
            } else if (result.isCancelled) {
              alert("Login was cancelled");
            } else {
              AccessToken.getCurrentAccessToken().then(data => {
                this.props.onLogin && this.props.onLogin(data.accessToken.toString());
              });
            }
          }}
          onLogoutFinished={() => {
            alert("User logged out");
            this.props.onLogout && this.props.onLogout();
          }}
        />
      </View>
    );
  }
}
