export default async token => {
  const response = await fetch(
    `https://graph.facebook.com/v2.5/me?fields=email,name,friends,picture.type(large)&access_token=${token}`
  );

  const { name, email, picture } = await response.json();

  const user = {
    username: name,
    name: name,
    email: email,
    fbImage: picture.data.url
  };

  return user;
};
