import NavigatorContainer from "./navigation/NavigatorContainer";

import React, { Component } from "react";
import { Provider } from "react-redux";
import { createStore } from "redux";
import reducer from "./reducers";
import Tts from 'react-native-tts';

const store = createStore(reducer);

export default class App extends Component {
  state = {
    isLoadingComplete: true
  };

  componentDidMount() {
    Tts.getInitStatus().then(() => Tts.setDefaultLanguage('en-IE'));
  }

  render() {
    return (
      <Provider store={store}>
        <NavigatorContainer />
      </Provider>
    );
  }
}
