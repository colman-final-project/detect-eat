import { SERVER_HOST } from 'react-native-dotenv';
import AsyncStorage from '@react-native-community/async-storage';

export const baseUrl = `http://${SERVER_HOST}:3000`;

export class Api {
  static post(path, body) {
    return fetch(baseUrl + path, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ ...body })
    })
      .then((response) => {
        if (response.status === 404) {
          throw Error('User name or password are incorrect');
        }

        return response.json();
      });
  }

  static async put(path, body) {
    const token = await AsyncStorage.getItem('localToken');

    return fetch(baseUrl + path, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify({ ...body })
    })
      .then((response) => {
        if (!response.ok) {
          throw Error(response);
        }
        return response.json();
      });
  }

  static get(path) {
    return fetch(baseUrl + path, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then((response) => response.json());
  }

  static delete(path) {
    return fetch(baseUrl + path, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then((response) => response.json());
  }

  static async me() {
    const token = await AsyncStorage.getItem('localToken');

    const response = await fetch(baseUrl + '/api/user/me', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    });

    if (response.status === 401) {
      await AsyncStorage.removeItem('localToken');

      throw Error(response.status);
    }

    return await response.json();
  }

  static async upload(path, body) {
    const token = await AsyncStorage.getItem('localToken');

    return fetch(baseUrl + path, {
      method: 'POST',
      headers: Object.assign({
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data'
      }, token ? { Authorization: `Bearer ${token}` } : {}),
      body
    })
      .then(response => {
        if (response.status === 404) {
          throw Error(response.status);
        }

        return response.json();
      });
  }
}
