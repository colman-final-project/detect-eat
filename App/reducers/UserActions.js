export const TYPES = {
  SET_USER: 'SET_USER',
  ADD_MEAL: 'ADD_MEAL',
  DELETE_MEAL: 'DELETE_MEAL',
  SET_SPEAK_MODE: 'SET_SPEAK_MODE'
};

export const setUser = user => ({ type: TYPES.SET_USER, user });

export const addMeal = meal => ({ type: TYPES.ADD_MEAL, meal });

export const deleteMeal = mealId => ({ type: TYPES.DELETE_MEAL, mealId });

export const setSpeakMode = speakMode => ({ type: TYPES.SET_SPEAK_MODE, speakMode });