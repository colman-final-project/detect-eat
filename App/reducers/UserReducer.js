import { TYPES } from "./UserActions";

const INITIAL_STATE = {
  username: "",
  name: "",
  lastName: "",
  email: "",
  password: "",
  image: "",
  meals: [],
  speakMode: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TYPES.SET_USER:
      return { ...state, ...action.user };
    case TYPES.ADD_MEAL:
      return { ...state, meals: [action.meal, ...state.meals] };
    case TYPES.DELETE_MEAL:
      return {
        ...state,
        meals: state.meals.filter(_ => _._id !== action.mealId)
      };
    case TYPES.SET_SPEAK_MODE:
      return { ...state, speakMode: action.speakMode };
    default:
      return state;
  }
};
