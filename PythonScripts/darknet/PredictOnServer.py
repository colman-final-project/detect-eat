import os
import subprocess
import base64
import sys
from subprocess import Popen, PIPE

IMAGE_TO_PREDICT = sys.argv[1]
EXE_OPEN = "./darknet"
MACHINE_FUNC = "detector test"
FOOD100_DATA_SRC = "food100.data"
WEIGHTS = "‏‏yolov2-food100_final.weights"
CONFIG = "yolov2-food100.cfg"
PATH_TO_IMG = "My_amazing_food.jpg"   #"C:\cygwin64\home\imageToSave.jpg"
LOG_PATH = "C:/darknet/log.txt"
PIC_PATH_SERVER = "/server/uploads‏"

def GetArgsFromClient():

    abspath_image = os.path.abspath(PIC_PATH_SERVER + IMAGE_TO_PREDICT)
    return abspath_image

def build_command(path):
    command = "./darknet detector test data\\food100\\food100.data cfg\\yolov2-food100.cfg " \
              "backup\\‏‏yolov2-food100_final.weights " + path + " 2<{}1 | tee -a log.txt".format("$")
    return command

def detect():
    img = GetArgsFromClient()
    cmd = ["bash", "-c",'cd /darknet; ./darknet detector test data/food100/food100.data cfg/yolov2-food100.cfg backup/yolov2-food100.weights {} > log.txt; exit 123'.format(img)]
    ret = subprocess.call(cmd)

def GoToDarknetFolder():
    command = "cd ..//..//..//..//"
    os.chdir(r"C:\cygwin64\bin")
    cmd = ["bash", "-c", "" + command + ";"]
    ret = subprocess.call(cmd)
    darknet_path = "C:\\darknet"
    command = "cd {}".format(darknet_path)
    os.chdir(r"C:\cygwin64\bin")
    cmd = ["bash", "-c", "{};".format(command)]
    ret = subprocess.call(cmd)

    return ret

def GoToFolder():
    command = "cd ..//..//..//..//"
    os.chdir(r"C:\cygwin64\bin")
    cmd = ["bash", "-c", "" + command + ";"]
    ret = subprocess.call(cmd)
    darknet_path = "C:\\darknet"
    command = "cd {}".format(darknet_path)
    os.chdir(r"C:\cygwin64\bin")
    cmd = ["bash", "-c", "{};".format(command)]
    ret = subprocess.call(cmd)

def ActivateDetection():
    path = "C:\\darknet\\"
    path += PATH_TO_IMG
    os.chdir(r"C:\cygwin64\bin")
    command = build_command(path)
    print("The image path is: {} ".format(path))
    print("the command is: {} ".format(command))
    os.chdir(r"C:\cygwin64\bin")
    cmd = ["bash", "-c", "cd C:\\darknet; {}; exit;".format(command)]
    ret = subprocess.call(cmd)


def SendToClient():
    return PredictionBoxes()

def PrintOnClient():
    print(PredictionResults())

def PredictionResults():
    predictions = os.path.abspath("/darknet‏/Predictions.jpg")
    with open(predictions, "rb") as imageFile:
        str = base64.b64encode(imageFile.read())

    imageFile.close()
    return str


def PredictionBoxes():
    current_dir = os.path.dirname(os.path.abspath(__file__))
    log = "log.txt"
    with open(current_dir + "//" + log) as f:
        content = f.read().splitlines()

    all_foods = []
    for line in content:
        if line.endswith('%'):
            curr_food = line.split(":")[0]
            all_foods.append(curr_food)

    return all_foods


# def get_max():
#
#     # Current directory
#     current_dir = os.path.dirname(os.path.abspath(__file__))
#     log = "log.txt"
#
#     with open(current_dir + "//" + log) as f:
#         content = f.read().splitlines()
#
#     i = 0
#     detect_food = []
#
#     for line in content:
#         i += 1
#         if line.find('Predicted in') != -1:
#             detect_food = content[i:]
#
#     i = 0
#     food_dic = {}
#     key = ""
#
#     for curr_food in detect_food:
#         i += 1
#         if i % 2 != 0:
#             key = str(curr_food).lower()
#             if not key in food_dic.keys():
#                 food_dic[key] = 0
#         else:
#             val, per = str(curr_food).split("%")
#             pre, val = str(val).split(": ")
#             val = int(val, base=10)
#             if key in food_dic.keys():
#                 if food_dic[key] < val:
#                     food_dic[key] = val
#
#     key_max = max(food_dic.keys(), key=(lambda k: food_dic[k]))
#     print(key_max)
#
#
# def go_to_folder():
#     command = "cd ..//..//..//..//"
#     os.chdir(r"C:\cygwin64\bin")
#     cmd = ["bash", "-c", "" + command + ";"]
#     ret = subprocess.call(cmd)
#     darknet_path = "C://100foods//darknet"
#     command = "cd {}".format(darknet_path)
#     os.chdir(r"C:\cygwin64\bin")
#     cmd = ["bash", "-c", "{};".format(command)]
#     ret = subprocess.call(cmd)
#
#     return ret
#
# def predict(image = "My_amazing_food4"):
#     #ret = go_to_folder()
#     path = os.path.dirname(os.path.abspath(__file__)) + "\\"
#     path += image
#     command = build_command(path)
#     print("The image path is: {} ".format(path))
#     print("the command is: {} ".format(command))
#     os.chdir(r"C:\cygwin64\bin")
#     cmd = ["bash", "-c", "{}; exit;".format(command)]
#     ret = subprocess.call(cmd)
#     print(get_max())
#

def main():
    # print("All Right lets try")
    # GoToDarknetFolder()
    # ActivateDetection()
    detect()
    SendToClient()

if __name__ == '__main__':
    main()


 #./darknet detector test data/food100/food100.data data/food100/yolov2-food100.cfg backup/yolov2-food100_100.weights My_amazing_food.jpg 2<&1 | tee -a log.txt;


