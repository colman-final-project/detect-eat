
import glob, os



# Current directory
current_dir = os.path.dirname(os.path.abspath(__file__))
log = "log.txt"

with open(log) as f:
    content = f.read().splitlines()

i = 0
detect_food = []

for line in content:
    i += 1
    if line.find('Predicted in') != -1:
        detect_food = content[i:]

i = 0
food_dic = {}
key = ""

for curr_food in detect_food:
    i += 1
    if i % 2 != 0:
        key = str(curr_food).lower()
        if not key in food_dic.keys():
            food_dic[key] = 0
    else:
        val, per = str(curr_food).split("%")
        pre, val = str(val).split(": ")
        val = int(val, base=10)
        if key in food_dic.keys():
            if food_dic[key] < val:
                food_dic[key] = val

key_max = max(food_dic.keys(), key=(lambda k: food_dic[k]))
print(key_max)
